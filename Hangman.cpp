/*Napravite jednostavnu igru vješala. Pojmovi se učitavaju u listu iz datoteke, i u svakoj partiji 
 * se odabire nasumični pojam iz liste. Omogućiti svu funkcionalnost koju biste očekivali od takve igre. 
 * Nije nužno crtati vješala, dovoljno je na labeli ispisati koliko je pokušaja za odabir slova preostalo.*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OOP_LV6_Hangman
{
    public partial class Form1 : Form
    {
        int i = 0;
        string rijec;
        char slovo;
        bool pogodak = false;
        int pokusaji = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnGen_Click(object sender, EventArgs e)
        {
            string[] lines = File.ReadAllLines(@"E:\Rijeci.txt");
            Random rand = new Random();

            StreamReader sr = new StreamReader(@"E:\Rijeci.txt");
            while (sr.ReadLine() != null)
                i++;

            sr = new StreamReader(@"E:\Rijeci.txt");

            for (int k = 0; k < rand.Next(1, i - 1); k++)
                rijec = sr.ReadLine();

            lblZvj.Text = "";
            for(i=0; i<rijec.Length; i++)
                lblZvj.Text = lblZvj.Text.Insert(i, "*");

            lblPreostali.Text = (7).ToString();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            slovo = textGuess.Text[0];

            for(i=0; i < rijec.Length; i++)
            {
                if(Char.ToUpper(rijec[i])==Char.ToUpper(slovo))
                {
                    pogodak = true;
                    lblZvj.Text = lblZvj.Text.Remove(i, 1);
                    lblZvj.Text = lblZvj.Text.Insert(i, slovo.ToString());
                }
            }

            if (lblZvj.Text.ToUpper() == rijec.ToUpper())
                Pobjeda();

            if (!pogodak)
            {
                pokusaji++;
                lblPreostali.Text = (7 - pokusaji).ToString();
                if (pokusaji == 7)
                    Gubitak();
            }
            pogodak = false;
        }

        private void Gubitak()
        {
            MessageBox.Show("Izgubili ste!");
            NoviGame();
        }

        private void Pobjeda()
        {
            MessageBox.Show("Pobijedili ste!");
            NoviGame();
        }

        private void NoviGame()
        {
            textGuess.Text = "";
            lblPreostali.Text = ".";
            lblZvj.Text = "Nova igra?";
            rijec = "";
        }

        private void lblPreT_Click(object sender, EventArgs e)
        {

        }
    }
}
